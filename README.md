# Terraform 101 :man-gesturing-ok:

**Introduction**
Terraform is part of infrastructure as code. **Infrastructure as code (IaC)** is the process of managing and provisioning computer data centers through machine-readable definition files, rather than physical hardware configuration or interactive configuration tools. Your infrastructure is code, the rest is abstracted by our cloud providerrs as they provide Infrastructure as a Service (IaaS).
In IAC, you have configuration management and orchestration tools.

**Configuration Managment**
Setting up indivisual servers to perfect desired state.
Or managing thier mutations.
- Chef
- Ansible
- Shell
- Puppet
  
**Orchestration Managment Tools**
These manages the networking and actual infrastructure (size of machines, subnets, VPC, NACL, SG, IGW, Routes). And deploy instances (machines) into these from AMIs (for example). They can also run small init script (User Data)

- Terraform
- Cloudformation (aws terraform)

# Terraform Objective
The objective of terraform **is to launch VPCs, Launch AMIs** (including running init scripts and start services)


**Example Terraform usage**
- Create VPC
- Setup SG, NCL and subnets
- Deploys AMI's into respective subnets
- Associate SG to instances
- Runs init scripts


**Example usage in DevOps**
- Origin code exist in bitbucket/github
- Automation gets triggered from change in source
- CI pull code into Jenkins and runs tests
- Passing tests trigger next job of pipeline (CD)
- Jenkins uses Ansible to change machine to have new code
- Jenkins uses Packer to deliver an AMI
- Jenkins uses Terraform to deploy AMI into production (CD)

## Terraform Main Commands
Terraform main commands are:
- terraform init

## Terraform main sections
- variables
- resources
- providers

## Installing Terraform

## Giving permissions
If running from your machine (thus not inside AWS) -  We need to give it some AWS secret keys and access keys.

Most clients will manage their keys differently depending of the level of DevSecOps sophistication.

## ⚠️ DON'T _bryson tiller voice_ ⚠️
- Hard code your keys into code. Ever
- Have the keys in a file that is .gitignore'd.

## Possible ways of key management
- Set them as environment variables for machine & interpolate (low level of sophistication)
- AIM roles (if you are running in the cloud)
- 

