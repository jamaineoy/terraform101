# Provider
## define where to create rersources and how to authenicate yourself.
## Options: get keys from vault, from ENV or leave blank and assume AWS role (machine needs to be set with IAM role for this)


provider "aws" {
    region = "eu-west-1"
    #access_key = "my-access-key"
    #secret_key = "my-secret-key"
}

variable "HTTPD_AMI" {
  type = string
}
# Resources
## Resources map to logical or physical things in a cloud provider
## For example a ec2 instance of a SG (logical security group)

resource "aws_instance" "cohort7_jamaine_httpd_tf" {
  #ami = "ami-0635ca8333e27394d"
  ami = var.HTTPD_AMI
  instance_type = "t2.micro"
  subnet_id = "subnet-953a58cf"
  key_name = "JaSSKey"
  associate_public_ip_address = true
  security_groups = [ "sg-091c83d3ecee7301e", "sg-01f897dc42d10c7de", aws_security_group.sg_terraform_jamaine_app.id, "sg-00af08d677ee563ee" ]
  tags = {
    "Name" = "cohort7_jamaine_httpd_tf"
  }

  # provisioner "file" {
  #   source      = "prov_phpapp.sh"
  #   destination = "/home/ec2-user/prov_phpapp.sh"
  # }


  # provisioner "remote-exec" {
  #     inline = [
  #     "chmod +x /home/ec2-user/prov_phpapp.sh",
  #     "/home/ec2-user/prov_phpapp.sh",
  #   ]
  #   connection {
  #   user = "root"
  #   type = "ssh"
  # }
  # }


}

resource "aws_security_group" "sg_terraform_jamaine_app" {
    name = "sg_terraform_jamaine-app"
    tags = {
      "Name" = "sg_terraform_jamaine-app"
    }
    vpc_id = "vpc-4bb64132"
    ingress {
      cidr_blocks = [ "81.152.186.104/32" ]
      description = "allows port 22 from my ip"
      from_port = 22
      protocol = "tcp"
      to_port = 22
    }
    ingress {
      cidr_blocks = [ "172.31.22.250/32" ]
      description = "allows port 22 from ansible controller"
      from_port = 22
      protocol = "tcp"
      to_port = 22
    }
    ingress {
      cidr_blocks = [ "0.0.0.0/0" ]
      description = "allows port 80 from world"
      from_port = 80
      protocol = "tcp"
      to_port = 80
    }
    ingress {
      cidr_blocks = [ "0.0.0.0/0" ]
      description = "allows port 443 from world"
      from_port = 443
      protocol = "tcp"
      to_port = 443
    }

    egress {
      cidr_blocks = [ "0.0.0.0/0" ]
      from_port = 0
      ipv6_cidr_blocks = [ "::/0" ]
      protocol = "-1"
      to_port = 0
    }
  
}